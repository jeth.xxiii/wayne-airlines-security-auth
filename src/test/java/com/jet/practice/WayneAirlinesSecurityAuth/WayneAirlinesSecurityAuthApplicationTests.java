package com.jet.practice.WayneAirlinesSecurityAuth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WayneAirlinesSecurityAuthApplicationTests {
	
	@Autowired
	private UserDetailsService userService;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testUserDetails() {
		UserDetails details =userService.loadUserByUsername("j.torres");
		System.out.println(details.toString());
	}

}
