package com.jet.practice.WayneAirlinesSecurityAuth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WayneAirlinesSecurityAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(WayneAirlinesSecurityAuthApplication.class, args);
	}
}
